#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
import cv2
import time
import math
import easygopigo as go
import gopigo as go2

# for code to work faster - comment image showing functions
# cv2.drawKeypoints, cv2.arrowedLine and cv2.imshow

gospeed = 100

#Hsv for disposal zone
lowerLimits_disposal = np.array([0, 47, 24])
upperLimits_disposal = np.array([255, 15, 255])
# HSV tresholds
lowerLimits_red = np.array([161, 80, 80])
upperLimits_red = np.array([179, 255, 255])

lowerLimits_green = np.array([52, 52, 52])
upperLimits_green = np.array([85, 255, 255])

lowerLimits_blue = np.array([100, 65, 65])
upperLimits_blue = np.array([126, 255, 255])


blur_filter = 1

def white_edge(th_img):
    th_img[0:,0] = 255
    th_img[0,0:] = 255
    th_img[0:,-1] = 255
    th_img[-1,0:] = 255
    
    return th_img

def list_objects(key_red, key_green, key_blue):
    obj_xysc = []
    key_all = [key_red, key_green, key_blue]
    # save points in one list
    for i in range(len(key_all)):
        key_tmp = key_all[i]
        if len(key_tmp)>0:
            for kp in key_tmp:
                x = kp.pt[0]
                y = kp.pt[1]
                s = kp.size
                new_pt = (int(x),int(y),s,i)# last value 'i' used for undersatanding color
                obj_xysc.append(new_pt)
                
    return obj_xysc
        
    

def sort_size(obj_list):
    nr_of_obj = len(obj_list)
    
    if nr_of_obj > 1:
        # list all object sizes
        obj_size = np.zeros(nr_of_obj)
        
        for i in range(nr_of_obj):
            obj_size[i] = obj_list[i][2]
        
        # sort descending based on size
        idx = np.argsort(-obj_size)
        #print(obj_size)
        #print(idx)

        # create an ordered array
        obj_list_new = list(obj_list)
        
        for i in range(nr_of_obj):
            obj_list_new[i] = obj_list[idx[i]]
        
        return obj_list_new
    else:      
        return obj_list
    

def main():

    my_robot = go.EasyGoPiGo()
    my_robot.set_speed(gospeed)
    
    blobparams = cv2.SimpleBlobDetector_Params()
    blobparams.filterByArea = True
    blobparams.minArea = 100
    blobparams.maxArea = 80000

    blobparams.filterByCircularity = False
    #blobparams.minCircularity = 0.1

    blobparams.minDistBetweenBlobs = 40
    blobparams.filterByInertia = False
    #blobparams.minInertiaRatio = 0.5
    blobparams.filterByConvexity = False
    #blobparams.minConvexity = 0.5

    detector = cv2.SimpleBlobDetector_create(blobparams)

    # Open the camera
    cap = cv2.VideoCapture(0)
    #width = 180#320#640#1280
    #height = 90#180#360#720
    cap.set(3,320)
    cap.set(4,180)

    width  = cap.get(cv2.CAP_PROP_FRAME_WIDTH)   # float `width`
    height = cap.get(cv2.CAP_PROP_FRAME_HEIGHT)  # float `height`
    
    # limit width
    width_center = width/2
    height_center = height/2
    height_select = 1 # form 0 to 1
    height_low = int(height_center-(height_center*height_select))
    height_high = int(height_center+(height_center*height_select))
    #print(width)

    width_center_new = width_center
    height_center_new = (height_high-height_low)/2
    
    #max_dist = np.sqrt(np.power(width,2)+np.power(height,2))
    max_size = (width_center_new/2) # radisu of largest object

    #kernel_size = blur_filter*2+1
    
    cv2.namedWindow("Original") # do not delete, used to quit the program
    
    obj_counter = 0
    obj_close_flag = 0
    
    obj_size0 = 0
    obj_size1 = 0
    obj_size2 = 0
    obj_size3 = 0
    
    while True:
        # Read the image from the camera
        ret, frame = cap.read()
        #frame = cv2.resize(frame,dim)
        # Cut a strip from the full frame
        frame = frame[height_low:height_high,:,:]

        # BLUR frames - not needed - it is better to decrease camera resolution
        #frame = cv2.GaussianBlur(frame,(kernel_size,kernel_size),sigmaX=0)

        frame_bgr = frame.copy()
        frame_hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

        # Our operations on the frame come here
        thresholded_disposal = np.invert(cv2.inRange(frame_hsv, lowerLimits_disposal, upperLimits_disposal))
        
        thresholded_red = np.invert(cv2.inRange(frame_hsv, lowerLimits_red, upperLimits_red))
        thresholded_green = np.invert(cv2.inRange(frame_hsv, lowerLimits_green, upperLimits_green))
        thresholded_blue = np.invert(cv2.inRange(frame_hsv, lowerLimits_blue, upperLimits_blue))

        
        thresholded_disposal = white_edge(thresholded_disposal)
        
        thresholded_red = white_edge(thresholded_red)
        thresholded_green = white_edge(thresholded_green)
        thresholded_blue = white_edge(thresholded_blue)

        # from task 1
        keypoint_disposal = detector.detect(thresholded_disposal)
        
        keypoint_red = detector.detect(thresholded_red)
        keypoint_blue = detector.detect(thresholded_blue)
        keypoint_green = detector.detect(thresholded_green)
        
        img = frame_bgr.copy()
        img2 = cv2.drawKeypoints(img, keypoint_disposal, np.array([]), (0,0,255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
        
        img = cv2.drawKeypoints(img, keypoint_red, np.array([]), (0,0,255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
        img = cv2.drawKeypoints(img, keypoint_green, np.array([]), (0,255,0), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
        img = cv2.drawKeypoints(img, keypoint_blue, np.array([]), (255,0,0), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

        # list all points
            
        all_objects = list_objects(keypoint_red,keypoint_blue,keypoint_green)  
        if obj_counter ==0:
            
            
            
            cv2.imshow('Original', img)
                           
            if len(all_objects) > 0:
                #print(len(all_objects))
                all_objects = sort_size(all_objects)
                #print(all_objects)
                
                biggest_obj = all_objects[0]
                close_xy = biggest_obj[0:2]
                close_size = biggest_obj[2]
                # show line to the biggest object
                img = cv2.arrowedLine(img, (int(width_center_new), int(height_center_new)), close_xy, (0, 255, 255), 3)
                
                
                obj_mean_size = (obj_size0+obj_size1+obj_size2+obj_size2+close_size)/4
                
                obj_size0 = obj_size1
                obj_size1 = obj_size2
                obj_size2 = close_size
                
                if obj_mean_size < max_size:
                    # calculate distance
                    x = close_xy[0]
                    y = close_xy[1]
                    dist = np.sqrt(np.power(width_center_new-x,2)+np.power(height_center_new-y,2))
                    
                    if close_xy[0] < width_center_new:
                        dist = dist*(-1)
        
                    obj_loc = -((x-width_center_new)/width_center_new)    
        
                    kp = 35.0 # increase proportionality constan (kp) to make turning stronger
                    # the linelocation value already shows error (location-center)
                    e = obj_loc # error of movement
                    Pout = kp*e
                    dsp_left = int(gospeed-Pout)
                    dps_right = int(gospeed+Pout)
                    
                    print('L:%.2f R:%.2f Obj radius: %.2f | max %.2f' % (dsp_left,dps_right,obj_mean_size,max_size))
        
                    # gopigo2
                    my_robot.set_left_speed(dsp_left)
                    my_robot.set_right_speed(dps_right)
                    my_robot.forward()
                    obj_close_flag = 0
                else:
                    my_robot.stop()
                    if obj_close_flag == 0:
                        obj_close_flag = 1
                        obj_counter += 1
                        print('Object Nr: %d'% (obj_counter))
                        
                        
                        
                   
                    time.sleep(0.2)
                    go2.turn_right(15)
                    time.sleep(0.3)
                    my_robot.stop()
                        #print('stop')
                        # Do autonomous or search mode here
                    
                    
                

            else:
                my_robot.stop()
                obj_close_flag = 0
                #print('stop - no objects')
                
                time.sleep(0.2)
                go2.turn_left(6)
                time.sleep(0.1)
                my_robot.stop()
    #             if obj_counter >= 1:
    #                 break
            
        
            cv2.imshow('Original', img)
            # Display the resulting frame
            #cv2.imshow('Thresholded', thresholded)
        elif obj_counter==1:
            img2 = cv2.arrowedLine(img2, (int(width_center_new), int(height_center_new)), close_xy, (0, 255, 255), 3)
            if obj_size3 < max_size:
                # calculate distance
                x = close_xy[0]
                y = close_xy[1]
                dist = np.sqrt(np.power(width_center_new-x,2)+np.power(height_center_new-y,2))
                
                if close_xy[0] < width_center_new:
                    dist = dist*(-1)
    
                obj_loc = -((x-width_center_new)/width_center_new)    
    
                kp = 35.0 # increase proportionality constan (kp) to make turning stronger
                # the linelocation value already shows error (location-center)
                e = obj_loc # error of movement
                Pout = kp*e
                dsp_left = int(gospeed-Pout)
                dps_right = int(gospeed+Pout)
                
                print('L:%.2f R:%.2f Obj radius: %.2f | max %.2f' % (dsp_left,dps_right,obj_mean_size,max_size))
    
                # gopigo2
                my_robot.set_left_speed(dsp_left)
                my_robot.set_right_speed(dps_right)
                my_robot.forward()
                obj_close_flag = 0
            else:
                my_robot.stop()
                print("stop elif1")
            # Quit the program when 'q' is pressed
            if (cv2.waitKey(1) & 0xFF) == ord('q'):
                my_robot.stop()
                break
        

    #print(width)

#         while True:
#             ret, frame = cap.read()
#             #frame = cv2.resize(frame,dim)
#             # Cut a strip from the full frame
#             frame = frame[height_low:height_high,:,:]
#             # BLUR frames - not needed - it is better to decrease camera resolution
#             #frame = cv2.GaussianBlur(frame,(kernel_size,kernel_size),sigmaX=0)
#             frame_bgr = frame.copy()
#             frame_hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
#             thresholded_disposal = np.invert(cv2.inRange(frame_hsv, lowerLimits_disposal, upperLimits_disposal))
#             thresholded_disposal = white_edge(thresholded_disposal)
#             keypoint_disposal = detector.detect(thresholded_disposal)
#             img = frame_bgr.copy()
#             img = cv2.drawKeypoints(img, keypoint_disposal, np.array([]), (0,0,255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
#             kp = 35.0 # increase proportionality constan (kp) to make turning stronger
#             # the linelocation value already shows error (location-center)
#             obj_loc = -((x-width_center_new)/width_center_new)
#             e = obj_loc # error of movement
#             Pout = kp*e
#             dsp_left = int(gospeed-Pout)
#             dps_right = int(gospeed+Pout)
#             my_robot.set_left_speed(dsp_left)
#             my_robot.set_right_speed(dps_right)
#             my_robot.forward()
#         ## end disposal area
#             cv2.imshow('Original', img)
#             

    # When everything done, release the capture
    print('closing program')
    cap.release()
    cv2.destroyAllWindows()

if __name__ == "__main__":
    main()
