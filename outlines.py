import numpy as np
import cv2
import time
import math
import easygopigo as go
import gopigo as go2

# for code to work faster - comment image showing functions
# cv2.drawKeypoints, cv2.arrowedLine and cv2.imshow

gospeed = 60

# HSV tresholds
lowerLimits_base = np.array([0, 0, 0])
upperLimits_base = np.array([255, 86, 136])

def white_edge(th_img):
    th_img[0:,0] = 255
    th_img[0,0:] = 255
    th_img[0:,-1] = 255
    th_img[-1,0:] = 255
    
    return th_img

def list_objects(key_base):
    obj_xysc = []
    key_all = [key_base]
    # save points in one list
    for i in range(len(key_all)):
        key_tmp = key_all[i]
        if len(key_tmp)>0:
            for kp in key_tmp:
                x = kp.pt[0]
                y = kp.pt[1]
                s = kp.size
                new_pt = (int(x),int(y),s,i)# last value 'i' used for undersatanding color
                obj_xysc.append(new_pt)
                
    return obj_xysc

def sort_size(obj_list):
    nr_of_obj = len(obj_list)
    
    if nr_of_obj > 1:
        # list all object sizes
        obj_size = np.zeros(nr_of_obj)
        
        for i in range(nr_of_obj):
            obj_size[i] = obj_list[i][2]
        
        # sort descending based on size
        idx = np.argsort(-obj_size)
        #print(obj_size)
        #print(idx)

        # create an ordered array
        obj_list_new = list(obj_list)
        
        for i in range(nr_of_obj):
            obj_list_new[i] = obj_list[idx[i]]
        
        return obj_list_new
    else:      
        return obj_list
    

def main():
    gospeed = 80
    rando = 0
    rando2 = 0
    rando3 = 0
    my_robot = go.EasyGoPiGo()
    my_robot.set_speed(gospeed)
    
    blobparams = cv2.SimpleBlobDetector_Params()
    blobparams.filterByArea = True
    blobparams.minArea = 100
    blobparams.maxArea = 80000

    blobparams.filterByCircularity = False
    #blobparams.minCircularity = 0.1

    blobparams.minDistBetweenBlobs = 40
    blobparams.filterByInertia = False
    #blobparams.minInertiaRatio = 0.5
    blobparams.filterByConvexity = False
    #blobparams.minConvexity = 0.5

    detector = cv2.SimpleBlobDetector_create(blobparams)

    # Open the camera
    cap = cv2.VideoCapture(0)
    #width = 180#320#640#1280
    #height = 90#180#360#720
    cap.set(3,640)
    cap.set(4,360)

    width  = cap.get(cv2.CAP_PROP_FRAME_WIDTH)   # float `width`
    height = cap.get(cv2.CAP_PROP_FRAME_HEIGHT)  # float `height`
#     
#     # limit width
    width_center = width
    height_center = height
    height_select = 1 # form 0 to 1
    height_low = int(height_center-(height_center*height_select))
    height_high = int(height_center+(height_center*height_select))
    #print(width)

    width_center_new = width_center
    height_center_new = (height_high-height_low)/2
#     
#     #max_dist = np.sqrt(np.power(width,2)+np.power(height,2))
    max_size = (width_center_new/4) # radisu of largest object

    #kernel_size = blur_filter*2+1
    
    cv2.namedWindow("Original") # do not delete, used to quit the program
    
    obj_counter = 0
    obj_close_flag = 0
    
    # save size object for several frames
    obj_size0 = 0
    obj_size1 = 0
    obj_size2 = 0
    
    while True:
        # Read the image from the camera
        ret, frame = cap.read()
        #frame = cv2.resize(frame,dim)
        # Cut a strip from the full frame
        #here is the frame cropped, so we can see the black outerline
        frame = frame[340:360,0:640]

        # BLUR frames - not needed - it is better to decrease camera resolution
        #frame = cv2.GaussianBlur(frame,(kernel_size,kernel_size),sigmaX=0)

        frame_bgr = frame.copy()
        frame_hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

        # Our operations on the frame come here
        thresholded_base = np.invert(cv2.inRange(frame_hsv, lowerLimits_base, upperLimits_base))
        

        thresholded_base = white_edge(thresholded_base)
        

        # from task 1
        keypoint_base = detector.detect(thresholded_base)
               
        img = frame_bgr.copy()
        img = cv2.drawKeypoints(img, keypoint_base, np.array([]), (0,0,255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
        
        # list all points
        
        all_objects = list_objects(keypoint_base)
        if len(all_objects) > 0:
            #print(len(all_objects))
            all_objects = sort_size(all_objects)
            #print(all_objects)
            
            biggest_obj = all_objects[0]
            close_xy = biggest_obj[0:2]
            close_size = biggest_obj[2]
            # show line to the biggest object
            img = cv2.arrowedLine(img, (int(width_center_new), int(height_center_new)), close_xy, (0, 255, 255), 3)
            
            
            obj_mean_size = (obj_size0+obj_size1+obj_size2+obj_size2+close_size)/4
            
            obj_size0 = obj_size1
            obj_size1 = obj_size2
            obj_size2 = close_size
            
            if obj_mean_size < max_size:
                # calculate distance
                x = close_xy[0]
                y = close_xy[1]
                dist = np.sqrt(np.power(width_center_new-x,2)+np.power(height_center_new-y,2))
                
                if close_xy[0] < width_center_new:
                    dist = dist*(-1)
    
                obj_loc = -((x-width_center_new)/width_center_new)    
    
                kp = 15.0 # increase proportionality constan (kp) to make turning stronger
                # the linelocation value already shows error (location-center)
                e = obj_loc # error of movement
                Pout = kp*e
                dsp_left = int(gospeed-Pout)
                dps_right = int(gospeed+Pout)
                
                print('L:%.2f R:%.2f Obj radius: %.2f | max %.2f' % (dsp_left,dps_right,obj_mean_size,max_size))
    
                # gopigo2
#                 my_robot.set_left_speed(dsp_left)
#                 my_robot.set_right_speed(dps_right)
#                 my_robot.forward()
#                 obj_close_flag = 0
                time.sleep(0.1)
                rando2 = 1
            else:
                 my_robot.stop()
                 
                    
                    #print('stop')
                    # Do autonomous or search mode here
                    
                    
                
            

        else:
            my_robot.stop()
            if rando2 == 0:
#             obj_close_flag = 0
                go2.forward()
                time.sleep(0.02)
            else:
                print("borderline")
                my_robot.stop()
                
            #print('stop - no objects')
#             if rando2 == 0:
#                 time.sleep(0.01)
#                 go2.turn_left(15)
#                 time.sleep(0.07)
#                 my_robot.stop()
#             else:
#                 my_robot.stop()
#                 #time.sleep(0.2)


            
            

        cv2.imshow('Original', img)
        # Display the resulting frame
        #cv2.imshow('Thresholded', thresholded)

        # Quit the program when 'q' is pressed
        if (cv2.waitKey(1) & 0xFF) == ord('q'):
            my_robot.stop()
            break

    # When everything done, release the capture
    print('closing program')
    cap.release()
    cv2.destroyAllWindows()

if __name__ == "__main__":
    main()



