# Trash cleaning robot

## List of Members
* Matis Vahter
* Dariia Avota
* Matis Tõnisson
* Vladimir Gruzdev

## Overview


Our task is to construct a robot which will be able to collect the trash (designed by our group) by implementing the digital image processing techniques.The main goal of our project is to make our robot to proceed with several tasks in order to clean up the field from the trash. Our will be able to: recognise the trash on the field, collect the trash and dispose the trash to the collecting area. Also robot will stay autonomous while collecting the trash. There will be several methods applied and tasks devided between all the participants. Whole project will be based on using of GoPiGo robot. Main problem that the robot will have to deal with is that there will be trash all other the place in a 4x4 meter area while also being of different colors (blue, green and red) and the robot will have to collect at least 6 out of 10 pieces of trash in one go while also being able to dispose them in one go as well.

## Task Formulation

_Subtask A_: Developing the area mapping method.(Matis Tõnisson)

_Subtask B_: Solving the trash detection method. (Dariia and Vladimir)

_Subtask C_: Developing the autonomous mode.(Matis Vahter)

_Subtask D_: Developing the collecting plough (Vladimir)

## List of materials/equipment

| Name                    | Amount
|--------------------     |-------
| Arduino nano            | x1
| Power cable             | x1
| Raspberry Pi            | x1
| GoPiGo robot            | x1
| Voltage regualtor       | x1
| LED                     | x4
| Lipo Battery            | x1
| Servo motors            | x2
| Jumper Wires and Regular wires | undefined
| Camera                  | x1
| Screws and flexible plastic clamp | undefined
| Miscellaneous             | undefined



## Schedule

[Our Trello Workplace](https://trello.com/2122aroboticsig10/tables) 

[Our project poster](https://docs.google.com/presentation/d/1rlCNIDitNOw6oOqq7929A2BMkEWzm9ymptBuQmr6T7c/edit?usp=sharing)

