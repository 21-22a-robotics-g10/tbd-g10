import numpy as np
import cv2
import time
import math
import easygopigo as go
import gopigo as go2
import DBZdef
import IPCdef
# for code to work faster - comment image showing functions
# cv2.drawKeypoints, cv2.arrowedLine and cv2.imshow


# HSV tresholds
lowerLimits_base = np.array([0, 0, 205])
upperLimits_base = np.array([255, 255, 255])

def white_edge(th_img):
    th_img[0:,0] = 255
    th_img[0,0:] = 255
    th_img[0:,-1] = 255
    th_img[-1,0:] = 255
    
    return th_img

def list_objects(key_base):
    obj_xysc = []
    key_all = [key_base]
    # save points in one list
    for i in range(len(key_all)):
        key_tmp = key_all[i]
        if len(key_tmp)>0:
            for kp in key_tmp:
                x = kp.pt[0]
                y = kp.pt[1]
                s = kp.size
                new_pt = (int(x),int(y),s,i)# last value 'i' used for undersatanding color
                obj_xysc.append(new_pt)
                
    return obj_xysc

def sort_size(obj_list):
    nr_of_obj = len(obj_list)
    
    if nr_of_obj > 1:
        # list all object sizes
        obj_size = np.zeros(nr_of_obj)
        
        for i in range(nr_of_obj):
            obj_size[i] = obj_list[i][2]
        
        # sort descending based on size
        idx = np.argsort(-obj_size)
        #print(obj_size)
        #print(idx)

        # create an ordered array
        obj_list_new = list(obj_list)
        
        for i in range(nr_of_obj):
            obj_list_new[i] = obj_list[idx[i]]
        
        return obj_list_new
    else:      
        return obj_list
    
## IPC(KP,SPEED)
## dbz(kp)
trashcollected = 0
while trashcollected <= 2:
    IPCdef.IPC(8,60)
    # IPC finds and collects
    trashcollected += 1
DBZdef.DBZ(15)
#DBZ is going to the base
# Black line detection should probly go into IPC code imo(change my mind or change the code)

# code might not be ready for showing yet.
